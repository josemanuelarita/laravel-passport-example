<?php

use Illuminate\Http\Request;
//use Symfony\Component\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//default
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//public Routes
Route::name('auth')->group(function () {
    Route::get('unauthorised', 'ApiAuthController@unauthorised');
    Route::post('register', 'ApiAuthController@register')->name('register');
    Route::post('login', 'ApiAuthController@login')->name('login');
});

//private Routes
Route::middleware('auth:api')->group(function () {
    Route::get('logout', 'ApiAuthController@logout')->name('logout');
});
